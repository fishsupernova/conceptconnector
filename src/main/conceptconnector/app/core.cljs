
; core.cljs

; SPDX-FileCopyrightText: 2022 Alexander Murphy <supernova@alexmurphy.uk>
; SPDX-FileCopyrightText: 2022 Joseph Corneli <holtzermann17@gmail.com>
; 
; SPDX-License-Identifier: EPL-2.0


(ns conceptconnector.app.core
  (:require [reagent.dom :as rdom]
            [reagent.core :as r]
            [clojure.string :as str]))

;;
;; Above this is boilerplate
;;

;;; Copy-paste from Clojure implementation

(def shelf (atom []))

(defn paragraph-to-words [text]
  (into #{} (distinct (str/split (str/replace text #"[^a-zA-Z ]" "") #" "))))

(defn similarity-of-texts [text1 text2]
  (count (clojure.set/intersection (paragraph-to-words text1) (paragraph-to-words text2))))

(defn similarity-of-sets [set1 set2]
  (count (clojure.set/intersection set1 set2)))

(defn store-text [text]
  (let [my-bag (paragraph-to-words text)]
    (swap! shelf conj {:hash (hash-string text) :bag my-bag :text text
                       :scores (into []
                                     (map (fn [them] {:key (:hash them) :score (similarity-of-sets my-bag (:bag them))})
                                          @shelf))
                       :valid false})))

(defn do-map [f & lists] (apply mapv f lists) nil)

(defn reindex-bags []
  (let [checkout @shelf]
    (reset! shelf
            (map
              (fn [item]
                (assoc item
                       :scores
                       (into [] (map (fn [other] {:key (:hash other) :score (similarity-of-sets (:bag item)
                                                                                                (:bag other))})
                                     (remove #(= (:hash %) (:hash item)) checkout)))
                       :valid true))
              checkout))))

(defn list-most-similar-documents []
  (let [checkout @shelf]
    (map
      (fn [item]
        [(:hash item)
         (:key (apply max-key :score (:scores item)))])
      checkout)))

(defn find-first [f coll] (first (filter f coll)))

(defn print-most-similar-documents []
  (let [checkout @shelf]
    (map
      (fn [item]
        [(:text item)
         (:text (find-first
                  #(= (:hash %) (:key (apply max-key :score (:scores item))))
                  checkout))]
        )
      checkout)))

(defn retrieve-similar [text]
  (let [checkout @shelf
        my-bag (paragraph-to-words text)
        my-scores (into [] (map (fn [other] {:key (:hash other) :score (similarity-of-sets my-bag (:bag other))})
                                checkout))]
    (:text (find-first
             #(= (:hash %) (:key (apply max-key :score my-scores)))
             checkout))))


; wasn't used, lazy
; (def known-concepts
;   "Persistent data, used as a crude database."
;   (r/atom (hash-map "dog" "Dogs are friendly four legged mammals." 
;                     "snake" "Snakes are unfriendly no-legged reptiles.")))

;;; New functionality

;; These are globals now, it didn’t work when they were scoped inside of chatlog

(def chats (r/atom []))
(def chatctr (r/atom 0))

;; This will show the wrong total count as a derived variable
(def character-count (reagent.ratom/reaction (count (apply str @chats))))

(defn send-chat! [text log]
  (js/alert (str "sending " @text))
  (swap! @log conj @text)
  )

(defn chat-input [value]
  [:textarea
   {:value        @value
    :on-change    #(reset! value (.. % -target -value))
    :on-key-press (fn [e]
                    (when (and (= (.-charCode e) 13) (.-shiftKey e))
                      (do
                        (.preventDefault e)
                        (swap! chatctr inc)
                        ;; Integrate simple NLP
                        (store-text @value)
                        (reindex-bags)
                        ;;
                        (swap! chats conj @value)
                        (reset! value ""))))}])

(defn chatlog
  "Function to match and display result of concept connections."
  []
  (let [lval (r/atom "")]
    [:div
     [:p "Press Shift+Enter to send a chat:"]
     [chat-input lval]
     [:p "Total # of chats: " @chatctr]
     [:p "Total # of characters: " @character-count]
     [:p "Chat log:" [:ol
                      (for [x (reverse @chats)]
                        [:li (str x "≈" (retrieve-similar x))])]]
     [:p "Actions:" [:ol
                     [:li [:button {:on-click (fn [e] (reset! chats (rest @chats)))} "Remove first!"]]
                     [:li [:button {:on-click (fn [e] (reset! chats (butlast @chats)))} "Remove last!"]]
                     [:li [:button {:on-click (fn [e] (reset! chats [])
                                                (reset! chatctr 0))} "Erase history!"]]]]
     ]))


;;; expanding textbox

(defn update-rows
  [row-count-atom max-rows dom-node value]
  (let [field-height   (.-clientHeight dom-node)
        content-height (.-scrollHeight dom-node)]
    (cond
      (and (not-empty value)
           (> content-height field-height)
           (< @row-count-atom max-rows))
      (swap! row-count-atom inc)

      (empty? value)
      (reset! row-count-atom 1))))


;;; old code

(defn textentry 
  "Textbox entry on site."
  [value]
  [:textarea {:type "text"
              :value @value
              :on-change #(reset! value (-> % .-target .-value))}])

(defn conceptmatch 
  "Function to match and display result of concept connections."
  []
  (let [val (r/atom "foo")]
    (fn []
      [:div
       [:p "Enter a concept: " [textentry val]]
       [:p "You entered: " @val]
       [:p (cond (clojure.string/includes? @val "dog")
                 (str "I know that one! Dogs are friendly four legged mammals. (" (hash-string "dog") ")")
                 (clojure.string/includes? @val "cat")
                 (str "I know that one! Cats are friendly four legged mammals. (" (hash-string "cat") ")")
                 (clojure.string/includes? @val "opossum")
                 (str "I know that one! Opossums are friendly four legged mammals. (" (hash-string "opossum") ")")
                 :else
                 (str "That's not a recognisable concept. (" (hash-string @val) ")"))]])))

(defn app []
  [:div
   [:h1 "Enter stuff to (rudimentarily) connect with concepts, try \"dog\"."]
   [chatlog]
   [conceptmatch]
   [:p "26th Oct 2022 Timestamp"]
   ])

;;
;; Below this is boilerplate
;;


(defn render []
  (rdom/render [app] (.getElementById js/document "root")))

(defn ^:export main []
  (render))

(defn ^:dev/after-load reload! []
  (render))
