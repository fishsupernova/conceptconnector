
<!-- 
SPDX-FileCopyrightText: 2022 Alexander Murphy <supernova@alexmurphy.uk>

SPDX-License-Identifier: CC-BY-4.0 
-->

# How to use super fast

Project is using 

- [Shadow-CLJS](http://shadow-cljs.org/) as the build tool / compiler

- [Reagent](https://github.com/reagent-project/reagent) (CLJS wrapper around [React](https://reactjs.org/)) for building your user interface

## Steps

Using your favorite shell:

`git clone` this repo

`cd` into the repo

`npm install` to download the dependencies locally (installed to the current dir)

`npm start` to get a local server up and running.

Open your web browser and go to `localhost:3000` 

